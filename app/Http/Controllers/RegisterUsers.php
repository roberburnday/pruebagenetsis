<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\User;
use App\preferencias;
use App\provincias;

class RegisterUsers extends Controller
{
    /**
     * 
     */
    public function showForm(){
        $provincias = provincias::all();
        $preferencias = preferencias::all();
        return view('register_form',[
            'provincias' => $provincias,
            'preferencias' => $preferencias,
        ]);
    }
    
    /**
     * 
     */
    public function saveForm(Request $request){
        $validator = User::getValidator();
        Validator::make($request->all(), $validator)->validate();
        //si se ha validado correctamente empezamos el transact de mysql para realizar la inserción
        DB::beginTransaction();
        //realizar la inserción de Usuario --> en caso de error lo notificamos con un dd
        $user = new User;
        $user->name = $request->name;
        $user->second_name = $request->second_name;
        $user->email = $request->email;
        if(!empty($request->address)){
            $user->address = $request->address;
        }
        $user->gender = $request->gender;
        $user->fk_province = $request->fk_province;
        $resultUser = $user->save();
        if(!$resultUser){
            DB::rollback();
            dd("Error insertando, aquí se haría un view que recogiera el error y lo enseñara");
        }else{
            //realizar la inserción (en batch) de usuarios_preferencias
            //--> en caso de error lo notificamos con un dd
            $preferencias_array = $request->preferencias;
            //crear aquí un array para el insert batch 
            $insertedPreferencias = $user->insertPreferencias($user->id, $preferencias_array);
            if(!$insertedPreferencias){
                DB::rollback();
                dd("Error insertando las preferencias, aquí se haría un view que recogiera el error y lo enseñara");
            }
        }
        //se notifica que todo esta bien, en principio estaríamos almacenando un data y lo pasaríamos al view
        DB::commit();
        dd("insertado",
           "se puede hacer más bonito con una vista y esas cosas, pero prefiero centrarme en la funcionalidad");
    }
    
    public function showDataAPI() {
        //cogemos los usuarios
        $users = User::select(
                    'users.*', 
                    DB::raw("GROUP_CONCAT(DISTINCT preferencias.preferencia SEPARATOR ',') as preferencias")
                )->join('usuarios_preferencias', 'users.id', '=', 'usuarios_preferencias.fk_user')
                ->join('preferencias', 'usuarios_preferencias.fk_preferencia', '=', 'preferencias.id_preferencia')
                ->groupBy('users.id')
                ->get();
        //TODO: posible mejora: si se quiere se puede sacar esa concatenación en un array, pero dependerá de la funcionalidad que desee el cliente
        //con un explode y un foreach se puede convertir en array
        
        //imprimimos el resultado en un json
        return response()->json($users);
    }
}
