{{-- en caso de ser un proyecto más grande ya heredaría de un layaout --}}

<form action="{{ route('form.post') }}" method="POST">
    @csrf
    <!-- formulario para dar de alta el usuario -->
    <label>
        Nombre *
        <input type="text" name="name" value="{{ old('name') }}"
               class="@error('name') is-invalid @enderror" required>
    </label> <br>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <label>
        Apellidos *
        <input type="text" name="second_name" value="{{ old('second_name') }}"
               class="@error('second_name') is-invalid @enderror" required>
    </label> <br>
        @error('second_name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <label>
        Email *
        <input type="email" name="email" value="{{ old('email') }}"
               class="@error('email') is-invalid @enderror" required>
        
    </label> <br>
        @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <label>
        Dirección
        <input type="text" name="address" value="{{ old('address') }}">
    </label> <br>
    <label>
        Sexo *
        <select name="gender" 
                class="@error('gender') is-invalid @enderror" required>
            <option value=""> -- </option>
            <option value="male" {{ (old("gender") == "male" ? "selected":"") }}>Hombre</option>
            <option value="female" {{ (old("gender") == "female" ? "selected":"") }}>Mujer</option>
        </select>
    </label> <br>
        @error('gender')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <label>
        Provincia *
        <select name="fk_province" class="@error('fk_province') is-invalid @enderror" required>
            <option value=""> -- </option>
            @foreach ($provincias as $provincia)
            <option value="{{$provincia->id_provincia}}" 
                    {{ (old("fk_province") == $provincia->id_provincia ? "selected":"") }}>
                {{ $provincia->provincia }}
            </option>
            @endforeach
        </select>
    </label> <br>
        @error('fk_province')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <hr>
    {{-- la maquetación la estoy dejando porque prefiero centrarme en la programación, pero eso sería cargando las css en el layaout y con distintas sections para editar o añadir --}}
    <p>Preferencias</p>
    @foreach ($preferencias as $preferencia)
    <label>
        <input type="checkbox" name="preferencias[]" 
                @if(is_array(old('preferencias')) && in_array($preferencia->id_preferencia, old('preferencias'))) 
                    checked 
                @endif
                value="{{$preferencia->id_preferencia}}">
        {{$preferencia->preferencia}}
    </label>
    @endforeach
    <hr>
    <input type="submit">
</form>