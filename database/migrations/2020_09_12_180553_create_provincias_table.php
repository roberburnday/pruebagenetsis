<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->increments('id_provincia');
            $table->string('provincia',30);
            $table->timestamps();
        });
        
        //inserts----------------------------
        DB::table('provincias')->insert(['id_provincia'=> 2,'provincia' => 'Albacete']);
        DB::table('provincias')->insert(['id_provincia'=> 3,'provincia' => 'Alicante/Alacant']);
        DB::table('provincias')->insert(['id_provincia'=> 4,'provincia' => 'Almería']);
        DB::table('provincias')->insert(['id_provincia'=> 1,'provincia' => 'Araba/Álava']);
        DB::table('provincias')->insert(['id_provincia'=> 33,'provincia' => 'Asturias']);
        DB::table('provincias')->insert(['id_provincia'=> 5,'provincia' => 'Ávila']);
        DB::table('provincias')->insert(['id_provincia'=> 6,'provincia' => 'Badajoz']);
        DB::table('provincias')->insert(['id_provincia'=> 7,'provincia' => 'Balears, Illes']);
        DB::table('provincias')->insert(['id_provincia'=> 8,'provincia' => 'Barcelona']);
        DB::table('provincias')->insert(['id_provincia'=> 48,'provincia' => 'Bizkaia']);
        DB::table('provincias')->insert(['id_provincia'=> 9,'provincia' => 'Burgos']);
        DB::table('provincias')->insert(['id_provincia'=> 10,'provincia' => 'Cáceres']);
        DB::table('provincias')->insert(['id_provincia'=> 11,'provincia' => 'Cádiz']);
        DB::table('provincias')->insert(['id_provincia'=> 39,'provincia' => 'Cantabria']);
        DB::table('provincias')->insert(['id_provincia'=> 12,'provincia' => 'Castellón/Castelló']);
        DB::table('provincias')->insert(['id_provincia'=> 51,'provincia' => 'Ceuta']);
        DB::table('provincias')->insert(['id_provincia'=> 13,'provincia' => 'Ciudad Real']);
        DB::table('provincias')->insert(['id_provincia'=> 14,'provincia' => 'Córdoba']);
        DB::table('provincias')->insert(['id_provincia'=> 15,'provincia' => 'Coruña, A']);
        DB::table('provincias')->insert(['id_provincia'=> 16,'provincia' => 'Cuenca']);
        DB::table('provincias')->insert(['id_provincia'=> 20,'provincia' => 'Gipuzkoa']);
        DB::table('provincias')->insert(['id_provincia'=> 17,'provincia' => 'Girona']);
        DB::table('provincias')->insert(['id_provincia'=> 18,'provincia' => 'Granada']);
        DB::table('provincias')->insert(['id_provincia'=> 19,'provincia' => 'Guadalajara']);
        DB::table('provincias')->insert(['id_provincia'=> 21,'provincia' => 'Huelva']);
        DB::table('provincias')->insert(['id_provincia'=> 22,'provincia' => 'Huesca']);
        DB::table('provincias')->insert(['id_provincia'=> 23,'provincia' => 'Jaén']);
        DB::table('provincias')->insert(['id_provincia'=> 24,'provincia' => 'León']);
        DB::table('provincias')->insert(['id_provincia'=> 27,'provincia' => 'Lugo']);
        DB::table('provincias')->insert(['id_provincia'=> 25,'provincia' => 'Lleida']);
        DB::table('provincias')->insert(['id_provincia'=> 28,'provincia' => 'Madrid']);
        DB::table('provincias')->insert(['id_provincia'=> 29,'provincia' => 'Málaga']);
        DB::table('provincias')->insert(['id_provincia'=> 52,'provincia' => 'Melilla']);
        DB::table('provincias')->insert(['id_provincia'=> 30,'provincia' => 'Murcia']);
        DB::table('provincias')->insert(['id_provincia'=> 31,'provincia' => 'Navarra']);
        DB::table('provincias')->insert(['id_provincia'=> 32,'provincia' => 'Ourense']);
        DB::table('provincias')->insert(['id_provincia'=> 34,'provincia' => 'Palencia']);
        DB::table('provincias')->insert(['id_provincia'=> 35,'provincia' => 'Palmas, Las']);
        DB::table('provincias')->insert(['id_provincia'=> 36,'provincia' => 'Pontevedra']);
        DB::table('provincias')->insert(['id_provincia'=> 26,'provincia' => 'Rioja, La']);
        DB::table('provincias')->insert(['id_provincia'=> 37,'provincia' => 'Salamanca']);
        DB::table('provincias')->insert(['id_provincia'=> 38,'provincia' => 'Santa Cruz de Tenerife']);
        DB::table('provincias')->insert(['id_provincia'=> 40,'provincia' => 'Segovia']);
        DB::table('provincias')->insert(['id_provincia'=> 41,'provincia' => 'Sevilla']);
        DB::table('provincias')->insert(['id_provincia'=> 42,'provincia' => 'Soria']);
        DB::table('provincias')->insert(['id_provincia'=> 43,'provincia' => 'Tarragona']);
        DB::table('provincias')->insert(['id_provincia'=> 44,'provincia' => 'Teruel']);
        DB::table('provincias')->insert(['id_provincia'=> 45,'provincia' => 'Toledo']);
        DB::table('provincias')->insert(['id_provincia'=> 46,'provincia' => 'Valencia/València']);
        DB::table('provincias')->insert(['id_provincia'=> 47,'provincia' => 'Valladolid']);
        DB::table('provincias')->insert(['id_provincia'=> 49,'provincia' => 'Zamora']);
        DB::table('provincias')->insert(['id_provincia'=> 50,'provincia' => 'Zaragoza']);

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provincias');
    }
}
