## Instalacion del passport

Las tablas estan incluidas en los migrations. Por lo que, con ejecutar:
> php artisan migrate

podrás disponer de todas las tablas necesarias para el passport.

Una vez creadas las tablas, deberás ejecutar el comando de instalación:
> php artisan passport:install

El instalador creará dos client's de pruebas. Para la ruta de la API no te sirven.

## Creacion de un client

Para crear un client tienes que ejecutar el comando:

> php artisan passport:client